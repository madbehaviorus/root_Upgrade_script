# Update and Upgrade Script as root crontab

## includes this packeges:

* sudo apt-get clean
* sudo apt-get update
* sudo apt-get upgrade
* sudo apt-get dist-upgrade
* sudo apt-get autoremove
* sudo apt-get autoclean

* open all in a screen task named "update"
* notify the currend user (not the root)


# HowTo

## Install


```
cd ~/Downloads
wget https://gitlab.com/madbehaviorus/root_Upgrade_script/raw/master/upgrade.sh
chmod +x upgrade.sh
# paste your username in line 18, save and exit
nano upgrade.sh
sudo mkdir /etc/cron/
sudo mkdir /var/log/cron
sudo cp upgrade.sh /etc/cron
sudo crontab -e


paste this in root crontab
```
# runs the script every third hour - 
57 */3 * * * /bin/bash --login /etc/cron/upgrade.sh >> /var/log/cron/upgrade.log 2>&1

``
save and exit

# logrotate
sudo nano /etc/logrotate.d/upgrade

paste this
```
/var/log/cron/upgrade.log {
      weekly
      rotate 12
      compress
      delaycompress
      missingok
      notifempty
      create 644 root root
}

```
save and exit

## What's the furture?


````
* ...hmm
* 
* Ideas? writing me ... .... .. .. 
* don't forget to encrypt with PGP:
* Fingerprint = "CB9D2966289388ABCD9ADE7BD08FF953629CD4C8"
````
