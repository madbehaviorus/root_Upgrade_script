#!/bin/bash

####################
## Mad Behaviorus ##
## 2019-10-08     ##
## Version 1.03   ##
####################


## Variables
# Colors
RED="\033[1;31m"
GREEN="\033[1;32m"
NOCOLOR="\033[0m"
# DBUS process name
USER_DBUS_PROCESS_NAME="gconfd-2"
# user
USER="insert_your_username_here"
# path to notify-send
NOTIFY_SEND_BIN="/usr/bin/notify-send"
# message title
TITLE="System"
# message him self
MESSAGE="Upgrade das System"
# message title2
TITLE2="System"
# message2 him self
MESSAGE2="System geupgradet"
# time
TIME=`date '+%H:%M:%S'`
DATE=`date '+%d-%m-%Y'`


## Begin message
# get pid of user dbus process
DBUS_PID=`ps ax | grep $USER_DBUS_PROCESS_NAME | grep -v grep | awk '{ print $1 }'`
# get DBUS_SESSION_BUS_ADDRESS variable
DBUS_SESSION=`grep -z DBUS_SESSION_BUS_ADDRESS /proc/$DBUS_PID/environ | sed -e s/DBUS_SESSION_BUS_ADDRESS=//`
# send notify
DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION su -c "$NOTIFY_SEND_BIN \"$TITLE\" \"$MESSAGE\"" $USER

## warning
echo -e "${GREEN}Date $DATE um $TIME Uhr ${NOCOLOR}"
echo
echo -e "${GREEN}     ####################${NOCOLOR}"
echo -e "${GREEN}     ## Mad Behaviorus ##${NOCOLOR}"
echo -e "${GREEN}     ## 2019-10-08     ##${NOCOLOR}"
echo -e "${GREEN}     ## Version 1.03   ##${NOCOLOR}"
echo -e "${GREEN}     ####################${NOCOLOR}"
echo -e "${GREEN}                         ${NOCOLOR}"
echo -e "${RED}     Bitte diese Fenster NICHT schliesen${NOCOLOR}"
echo
# clean apt cache
echo -e "Schritt 1: ${GREEN}update apt clean${NOCOLOR}"
sudo apt-get clean

echo
# update apt cache
echo -e "Schritt 2: ${GREEN}update apt cache${NOCOLOR}"
sudo apt-get update

echo
# upgrade packages
echo -e "Schritt 3: ${GREEN}upgrade packages${NOCOLOR}"
sudo apt-get upgrade -y

echo
# dist-upgrade
echo -e "Schritt 4: ${GREEN}distribution upgrade${NOCOLOR}"
sudo apt-get dist-upgrade -y

echo
# purge all removefull packages
echo -e "Schritt 5: ${GREEN}remove unused packages${NOCOLOR}"
sudo apt-get --purge autoremove -y

echo
# clean up installfiles
echo -e "Schritt 6: ${GREEN}clean up${NOCOLOR}"
sudo apt-get autoclean

echo

## Begin message
# get pid of user dbus process
DBUS_PID=`ps ax | grep $USER_DBUS_PROCESS_NAME | grep -v grep | awk '{ print $1 }'`
# get DBUS_SESSION_BUS_ADDRESS variable
DBUS_SESSION=`grep -z DBUS_SESSION_BUS_ADDRESS /proc/$DBUS_PID/environ | sed -e s/DBUS_SESSION_BUS_ADDRESS=//`
# send notify
DBUS_SESSION_BUS_ADDRESS=$DBUS_SESSION su -c "$NOTIFY_SEND_BIN \"$TITLE2\" \"$MESSAGE2\"" $USER



